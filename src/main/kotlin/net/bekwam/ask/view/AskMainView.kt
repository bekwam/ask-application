package net.bekwam.ask.view

import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.geometry.Side
import javafx.scene.layout.Priority
import net.bekwam.ask.styles.AskStyles
import tornadofx.*

/**
 * @author carl
 */

class KeyAssignView : View() {

    val spacing = 20.0

    override val root = gridpane {

        addClass(AskStyles.panel)

        hgap = spacing
        vgap = spacing
        padding = Insets(0.0, 20.0, 20.0, 20.0)

        row {
            button  {
                addClass(AskStyles.topButton)
            }
            button{
                addClass(AskStyles.topButton)
            }
            button{
                addClass(AskStyles.topButton)
            }
        }
        row {
            button  {
                addClass(AskStyles.lowerButton)
            }
            button{
                addClass(AskStyles.lowerButton)
            }
            button{
                addClass(AskStyles.lowerButton)
            }
        }
        row {
            button  {
                addClass(AskStyles.lowerButton)
            }
            button{
                addClass(AskStyles.lowerButton)
            }
            button{
                addClass(AskStyles.lowerButton)
            }
        }
        row {
            button  {
                addClass(AskStyles.lowerButton)
            }
            button{
                addClass(AskStyles.lowerButton)
            }
            button{
                addClass(AskStyles.lowerButton)
            }
        }
    }
}

class FileOpsView : View() {

    override val root = vbox {
        addClass(AskStyles.panel)
        padding = Insets(20.0)
        spacing = 20.0
        button("Load from Clipboard") {
            addClass( AskStyles.clipboardButton )
        }
        button("Copy Recipe to Clipboard") {
            addClass( AskStyles.clipboardButton )
            }
        button("Load Recipe") {
            addClass( AskStyles.fileButton )
        }
        button("Load Layout") {
            addClass( AskStyles.fileButton )
        }
    }
}

class KeyboardControlView : View() {
    override val root = hbox {
        alignment = Pos.CENTER
        padding = Insets(20.0)
        spacing = 20.0
        addClass(AskStyles.panel)

        button("Save") {
            addClass(AskStyles.commandButton)
        }
        button("Options") {
            addClass(AskStyles.commandButton)
        }
        button("Demo") {
            addClass(AskStyles.demoButton)
        }
        button("Upload All Changes") {
            addClass(AskStyles.longCommandButton)
        }
    }
}

class DesignerView : View() {
    override val root = vbox {
        vgrow = Priority.ALWAYS
        addClass(AskStyles.panel)
        label("DesignerView Content")
    }
}

class ToolboxView : View() {
    override val root = vbox {

        addClass(AskStyles.panel)

        tabpane {
            side = Side.LEFT
            tab("Keyboard Keys") {
                vbox {
                    isClosable = false
                    padding = Insets(20.0)
                    alignment = Pos.CENTER
                    spacing = 20.0
                    addClass(AskStyles.keyboardKeyPanel)
                    button("Letter Key") {
                        addClass(AskStyles.specialKey)
                    }
                    button("Number Key") {
                        addClass(AskStyles.specialKey)
                    }
                    button("Character Key") {
                        addClass(AskStyles.specialKey)
                    }
                    button("Shift")
                    button("Enter")
                    button("Tab")
                    button("Control")
                    button("Delete")
                    button("Alt")
                    button("Esc")
                    button("Backspace")
                    button("Windows Key")
                    button("Home Key")
                }
            }
            tab("Special") { isClosable = false }
            tab("Media") { isClosable = false }
            tab("Mouse") { isClosable = false }
            tab("F Keys") { isClosable = false }
            tab("NumPad") { isClosable = false }
        }
    }
}

class StatusView : View() {
    override val root = hbox {
        padding = Insets(4.0)
        spacing = 4.0
        progressbar()
        label("Status")
    }
}

class AskMainView : View("AskApp") {

    override val root = vbox {

        addClass(AskStyles.wrapper)

        borderpane {
            vgrow = Priority.ALWAYS
            padding = Insets(20.0)
            left = vbox {
                spacing = 20.0
                add(KeyAssignView::class)
                add(FileOpsView::class)
            }
            center = vbox {
                padding = Insets(0.0, 20.0, 0.0, 20.0)
                spacing = 20.0
                add(KeyboardControlView::class)
                add(DesignerView::class)
            }
            right = vbox {
                add(ToolboxView::class)
            }
        }
        separator()
        add(StatusView::class)
    }
}