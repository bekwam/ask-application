package net.bekwam.ask.app

import javafx.scene.Scene
import net.bekwam.ask.styles.AskStyles
import net.bekwam.ask.view.AskMainView
import tornadofx.App
import tornadofx.UIComponent
import tornadofx.reloadStylesheetsOnFocus

/**
 * @author carl
 */

class AskApp : App(AskMainView::class, AskStyles::class) {

    //override fun createPrimaryScene(view: UIComponent) = Scene( view.root, 1280.0, 800.0 )

    init {
        reloadStylesheetsOnFocus()
    }
}