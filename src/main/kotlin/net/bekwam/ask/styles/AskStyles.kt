package net.bekwam.ask.styles

import javafx.scene.paint.Color
import tornadofx.*


/**
 * @author carl
 */
class AskStyles : Stylesheet() {

    private val blackColor = Color.BLACK
    private val purpleColor = c("#9999ff")
    private val lightOrangeColor = c("#fff2e6")
    private val redColor = c("#ff471a")
    private val yellowColor = c("#ffcc00")
    private val greenColor = c("#00ff80")
    private val whiteColor = c("#ffffff")

    companion object {
        val wrapper by cssclass()
        val panel by cssclass()
        val topButton by cssclass()
        val lowerButton by cssclass()
        val clipboardButton by cssclass()
        val fileButton by cssclass()
        val commandButton by cssclass()
        val demoButton by cssclass()
        val longCommandButton by cssclass()
        val keyboardKeyPanel by cssclass()
        val specialKey by cssclass()
    }

    init {
        wrapper {
            backgroundColor += purpleColor
        }
        panel {
            backgroundColor += lightOrangeColor
            borderColor += box(
                   blackColor
            )
            borderWidth += box( 4.0.px )
            borderRadius += box( 10.0.px )
            unsafe("-fx-border-style", raw("solid outside"))
        }

        topButton {
            backgroundColor += redColor
            borderColor += box(blackColor)
            borderWidth += box( 4.0.px )
            borderRadius += box( 10.0.px )
            unsafe("-fx-border-style", raw("solid outside"))
            prefWidth = 80.0.px
            prefHeight = 80.0.px
        }

        lowerButton {
            backgroundColor += purpleColor
            borderColor += box(blackColor)
            borderWidth += box( 4.0.px )
            borderRadius += box( 10.0.px )
            unsafe("-fx-border-style", raw("solid outside"))
            prefWidth = 80.0.px
            prefHeight = 80.0.px
        }

        clipboardButton {
            backgroundColor += yellowColor
            borderColor += box(blackColor)
            borderWidth += box( 4.0.px )
            borderRadius += box( 10.0.px )
            unsafe("-fx-border-style", raw("solid outside"))
            prefWidth = 280.0.px
            prefHeight = 60.0.px

        }

        fileButton {
            backgroundColor += greenColor
            borderColor += box(blackColor)
            borderWidth += box( 4.0.px )
            borderRadius += box( 10.0.px )
            unsafe("-fx-border-style", raw("solid outside"))
            prefWidth = 280.0.px
            prefHeight = 60.0.px
        }

        commandButton {
            backgroundColor += purpleColor
            borderColor += box(blackColor)
            borderWidth += box( 4.0.px )
            borderRadius += box( 10.0.px )
            unsafe("-fx-border-style", raw("solid outside"))
            prefWidth = 120.0.px
            prefHeight = 60.0.px
        }

        longCommandButton {
            backgroundColor += purpleColor
            borderColor += box(blackColor)
            borderWidth += box( 4.0.px )
            borderRadius += box( 10.0.px )
            unsafe("-fx-border-style", raw("solid outside"))
            prefWidth = 240.0.px
            prefHeight = 60.0.px
        }

        demoButton {
            backgroundColor += whiteColor
            borderColor += box(blackColor)
            borderWidth += box( 4.0.px )
            borderRadius += box( 10.0.px )
            unsafe("-fx-border-style", raw("solid outside"))
            prefWidth = 240.0.px
            prefHeight = 30.0.px
        }

        keyboardKeyPanel {
            button {
                backgroundColor += purpleColor
                borderColor += box(blackColor)
                borderWidth += box( 4.0.px )
                borderRadius += box( 10.0.px )
                unsafe("-fx-border-style", raw("solid outside"))
            }
        }

        keyboardKeyPanel {
            specialKey {
                backgroundColor += redColor
            }
        }
    }
}
